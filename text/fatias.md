# Fatias (ou *slices*) de listas e de *strings*

## Ideia geral

As listas e cadeias de caracteres em Python podem ser "fatiadas", ou *sliced* -- o recurso da linguagem para isso é chamado **slicing**.

Ele permite, como o nome sugere, selecionar trechos de uma cadeia de caracteres ou de uma lista.

A forma geral, teórica do *slicing* é

```Python3
lista[inferior : superior : passo]
```

onde

* `inferior` é o limite inferior, o índice do primeiro elemento
    a ser incluído na fatia;

* `superior` é o limite superior, o índice do primeiro elemento
    que *não* será incluído.

* `passo` é o incremento entre índices, a partir do inferior.
    Muito pouco usado, quando não incluído, o `passo` é `1`.

Esses conceitos são muito simples, mas sua aplicação nem sempre é clara ou intuitiva. É preciso alguma prática para compreender sua aplicação. Para isso, acompanharemos vários exemplos.

Começaremos com uma lista de inteiros:

```Python3
>> a = [ind for ind in range(100)]
```

Eis sua visualização:

```Python3
>> print(a)
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99]
```

Caso se tente se use `99` como último índice, se tem

```Python3
>> print(a[0:99])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98]
```

Ou seja: o último índice foi uma posição antes do final. Veja

```Python3
>> print(a[0:10])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

Caso se varie o índice inferior, o resultado é diferente:

```Python3
>> print(a[2:10])
[2, 3, 4, 5, 6, 7, 8, 9]
```

Ou seja: o índice inferior é de fato seguido.

Assim, caso se deseje incluir o último elemento em uma fatia, deve-se ultrapassá-lo.

```Python3
>> print(a[0:100])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99]
```

Por qualquer quantidade -- apenas os tamanho real da lista será considerado:

```Python3
>> print(a[0:1000])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99]
```

Finalmente, um passo de `2` gera:

```Python3
print(a[0:1000:2])
[0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98]
```

Apenas os índices pares foram considerados.

Caso o limite inferior não seja `0`, tem-se, por exemplo:

```Python3
>> print(a[2:1000:3])
[2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 53, 56, 59, 62, 65, 68, 71, 74, 77, 80, 83, 86, 89, 92, 95, 98]
```

## Limites negativos

A notação de fatiamento, ou *slicing* permite um recurso pouco intuitivo: limites negativos.

Sabendo que em Python os índices são inteiros não negativos -- o zero e os valores positivos, que vão até uma posição antes do tamanho da lista. Por exemplo, na lista que estamos usando, com `100` elementos, o primeiro índice é `9` e o último índice é

`100 - 1 = 9`.

Mantendo o limite inferior como zero e definindo o limite superior como `-1`, temos

```Python3
>> print(a[0:-1])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98]
```

Ou seja: `-1` subtraiu um elemento da lista.

Para o limite superior igual a `-2`, temos:

```Python3
>> print(a[0:-2])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97]
```

O limite superior igual a `-2` fez subtrair dois elementos da lista.

O mesmo raciocínio se aplica para limite superior igual a `-3`:

```Python3
>> print(a[0:-3])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96]
```

Um raciocínio semelhante acontece para o limite inferior negativo:

```Python3
>> print(a[-1:100])
[99]
```

O limite inferior negativo é considerado o último elemento da lista. Caso se tenha `-2`, são o último e o penúltimo

```Python3
>> print(a[-2:100])
[98, 99]
```

De modo semelhante para `-3`.

```Python3
print(a[-3:100])
[97, 98, 99]
```

Uma mistura de limites negativos interessante é

```Python3
print(a[-2:-1])
[98]
```

Ela começa em `-2`, que é o penúltimo elemento da lista (no contexto do limite inferior), e vai até `-1`, que também é o penúltimo elemento da lista, no contexto do limite superior.

## Passos

Um efeito interessante é ter passos negativos:

```Python3
print(a[100:0:-1])
[99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
```

Os passos negativos permitem inverter a lista.

## Omissão de limites

## Muitos exemplos
