---
params:
  lang: PT
lang: "`r switch(params$lang, PT = 'pt-BR', EN = 'en-US')`"
title: "Carga dinâmica de módulos Python"
author: "Hilton G. Fernandes"
date: "2021-11-19"
output: 
  pdf_document: 
    keep_tex: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Módulos dinâmicos no Python

Programas Python em geral precisam de bibliotecas adicionais, que vão oferecer 
funcionalidades além daquelas expressas pelos arquivos do programa.  As 
funcionalidades adicionais usualmente estão em arquivos de módulos. Em muitos 
casos, esses módulos são carregados no início do programa, através de comandos 
`import`. 

Assim, o mesmo programa vai importar sempre os mesmos módulos, que estão 
definidos de modo **estático** -- isto é: seus nomes aparecem como constantes
no código; não podem ser alterados. 

Contudo, podem existir situações onde diferentes módulos tenham que ser 
carregados de modo dinâmico, de acordo com condições variáveis. Um exemplo 
atual seria um programa de *machine learning*, que poderá usar hardware 
especial para suas operações, caso ele esteja disponível; ou então usar o 
processador convencional caso contrário. 

No exemplo a seguir, segue um exemplo simplificado de como fazer carga 
dinâmica de módulos. Para obter foco na carga dinâmica, foram deixados 
deliberadamente de lado questões que têm que ser abordadas com cuidado em um 
projeto realista de carga dinâmica de módulos, especialmente o tratamento de 
erros. 

Neste exemplo simplificado, há apenas 4 arquivos: 

* `dyn_loading.py`, o programa principal, que faz a carga do módulo que, 
  no exemplo dado, ofereceria as funções para *machine learning*.
  
  Ele executa a função `func1()` do módulo `dyn_loader`;
  
* `__init__.py`, o programa de inicialização do módulo `dyn_loader`. Ele é um 
  arquivo no diretório `dyn_loader`.
  
  Ou seja: o módulo `dyn_loader` não é mais um arquivo, como usual em 
  estruturas mais simples de módulos. 

  Seu papel é carregar ou o módulo `one_module` ou o módulo `other_module`, 
  para oferecer a função `func1()`. Neste caso, a escolha por um módulo ou 
  outro  é aleatória, a depender do momento da execução;
  
* `one_module.py` e `other_module.py` são módulos que apenas oferecem uma 
  função `func1()`. A versão da função em cada módulo imprime o nome do módulo. 
  
# Listagem comentada dos arquivos 

## `dyn_loading.py`

```{python, attr.source='.numberLines', eval=FALSE, echo=TRUE, code=xfun::read_utf8('../src/dyn_loading.py')}
```

Na linha `9` é feita a carga estática do módulo `dyn_loader`, que fará a carga 
dinâmica de um dos dois módulos `one_module` e `other_module`

Na linha `11` é feita a chamada da função `func1()` que o módulo `dyn_loader` 
exporta. 

## `dyn_loader/__init__.py`

```{python, attr.source='.numberLines', eval=FALSE, echo=TRUE, code=xfun::read_utf8('../src/dyn_loader/__init__.py')}
```

`dyn_loader/__init__.py` é  o arquivo de inicialização do módulo `dyn_loader` e 
que cria variáveis globais a ele.

Nas linhas `9` e `10` são importados (de modo estático) os módulos `random` 
`importlib` que trarão funcionalidade a ser usada no programa.

Na linha `12` é inicializado o motor para a geração de números pseudoaleatórios 
do módulo `random`. Na linha `13` o motor de `random` escolhe ou o número `0` 
ou `1`.

Nas linhas de `17` a `20` é carregado dinamicamente ou o módulo `other_mod`
(quando `choice` vale `0`), ou o módulo `one_mod`, quando `choice` vale `1`.

Nos dois casos, as informações do módulo são atribuídas à variável `module`. 

Na linha `22`, a função `func1()`, presente tanto em `other_mod` quanto em 
`one_mod`, é atribuída à variável global `func1` de `dyn_loader`, que é 
exportada quando o módulo `dyn_loader` é carregado. 

Finalmente, na linha `24` é feita uma listagem dos identificadores exportados 
pelo módulo `dyn_loader`. 

## Saída de `dyn_loading.py`

```{python, attr.source='.numberLines', eval=FALSE, echo=TRUE, code=xfun::read_utf8('../src/dyn_loading.out')}
```

Nesta listagem da saída de `dyn_loading.py`, apenas a linha `5` foi gerada no
código `dyn_loading.py`, com a chamada de `dyn_loader.func1()`. As outras foram
geradas na chamada implícita de `dyn_loader/__init__.py`.

A linha `1` da saída de `dyn_loading.py` mostra que, nesta execução, foi 
escolhido o valor `1` para `choice`, causando assim a carga do módulo `one_mod`, 
como visto na linha `5` da listagem.

As linhas de `2` a `4` mostram todos identificadores exportados pelo módulo
`dyn_loader`. A rigor só estaríamos interessados em `func1`, mostrada na linha 
`3` deste grupo. Mas uma característica da linguagem Python é tornar públicos e
acessíveis os elementos de módulos e classes. 

Os elementos com duplo caracter de "traço baixo", ou *underscore*, são elementos
dos módulos mais internos do Python; são chamados de *dunder* no jargão do 
Python. As variáveis `choice` e `module`, de uso interno do programa, são 
exportadas sem necessidade. O diretório de identificadores exportados inclui
também os módulos `random` e `importlib`. 

## Arquivos `one_mod.py` e `other_mod.py`

Os arquivos `one_mod.py` e `other_mod.py`, que exportam respectivamente os 
módulos  `one_mod` e `other_mod`, não trazem nenhuma informação muito
importante: apenas imprimem o nome de seu módulo e da função `func1()` sendo 
exportada por eles.

Apenas por completude, segue a listagem de `one_mod.py`:

```{python, attr.source='.numberLines', eval=FALSE, echo=TRUE, code=xfun::read_utf8('../src/one_mod.py')}
```

