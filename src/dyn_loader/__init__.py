#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 15:01:41 2021

@author: hilton
"""

import random # seed(), getrandbits()
import importlib # import_module

random.seed(version = 2)
choice = random.getrandbits(1)

print(f'Chosen {choice} on __init__.py')

if choice == 0: 
    module = importlib.import_module('other_mod')
else: 
    module = importlib.import_module('one_mod') 

func1 = module.func1

print(dir())