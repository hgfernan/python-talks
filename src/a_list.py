#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 18 16:19:38 2021

@author: hilton
"""

a = [i for i in range(100)]

print(a)

print(a[0:99])

print(a[0:10])

print(a[2:10])

print(a[0:100])

print(a[0:1000])

print(a[0:1000:2])

print(a[1:1000:3])

print(a[2:1000:3])

print(a[0:-1])

print(a[0:-2])

# Use of slices to modify parts


b = [0, 1, 2, 3, 4]
print(b)

b[1:3] = []
print(b)

b = [0, 1, 2, 3, 4]
b[1:3] = [5, 6, 7, 8]
print(b)
